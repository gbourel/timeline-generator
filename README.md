# Timeline-generator

Small script for Timeline's board game like cards generation, using printerstudio.com's format.

---

Script simple pour la génération de cartes à jouer type "Timeline".

## Usage

### Dependencies

Two dependencies :
 * this script runs on [node.js](https://nodejs.org/)
 * it uses [Inkscape](https://inkscape.org/) to convert svg images to png.

### Command line

`node build.js <DECK_NAME>`

with DECK_NAME being one of available deck in "decks" subdirectory.

## Create a new deck

 1. Duplicate or create a new directory in "decks" subdirectory
 2. Update `data.csv` in created directory with cards data. CSV format is `Card's main text ; Year ; Image filename ; Optional sub-text`
 3. Put images files in deck's `img` sub-sub-directory.
 4. Run `node build.js <DECK_NAME>`

## Card format

For now the printerstudio's "miniEuropean" is used. Format may be change in build.js file.
