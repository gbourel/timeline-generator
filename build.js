const util = require('util');
const exec = util.promisify(require('child_process').exec);
const fs = require('fs');
const path = require('path');
const readline = require('readline');

/// Creates png image from svg.
async function exportCard(svg, dest) {
  let cmd = 'inkscape'
    + ' --export-png="' + dest + '"'
    + ' --export-dpi=300'
    + ' ' + svg
  const { stdout, stderr } = await exec(cmd);
  console.log('stdout:', stdout);
  console.log('stderr:', stderr);
}

/// Format filename replacing spaces and special characters.
function formatName(name) {
  let str = name.replace(/\s+/gi, '_');
  str = str.replace(/\'/gi, '');
  str = str.replace(/"/gi, '');
  str = str.replace(/\\n/gi, '_');
  return str;
}

/**
 * Define card's text.
 * @param content card's svg content
 * @param main main text (eg. "Invention of thermometer")
 * @param second optional sbu-text (eg. "By Galileo Galilei")
 * @param options optional options :
 *   - 'small_text': forces smaller text (allows long
 *                   descriptions)
 */
function putText(content, main, second, options) {
  let txt = content;
  let lines = main.split('\\n');
  let yFirst = '64.371973';
  let smallText = options && options.includes('small_text');

  if(lines.length > 2) {
    yFirst = '63.271973';
    // second line & third lines
    txt = txt.replace('line-height:125%;', 'line-height:100%;')
    txt = txt.replace('Card_Text</tspan>', 'Card_Text</tspan><tspan style="font-weight:bold;font-size:2.96333338px;line-height:100%;font-family:serif;-inkscape-font-specification:\'serif Bold\';text-align:center;text-anchor:middle;fill:#242424;fill-opacity:1;stroke-width:0.28222224px" x="32.372433" y="66.891973" sodipodi:role="line">' + lines[1] + '</tspan><tspan style="font-weight:bold;font-size:2.96333338px;line-height:100%;font-family:serif;-inkscape-font-specification:\'serif Bold\';text-align:center;text-anchor:middle;fill:#242424;fill-opacity:1;stroke-width:0.28222224px" x="32.372433" y="66.891973" sodipodi:role="line">' + lines[2] + '</tspan>');
    txt = txt.replace('Card_Text', lines[0]);
    // move text up
    txt =  txt.replace('66.271973', yFirst);
    if(!smallText) {
      txt = txt.replace(/font-size:2.96333338px;/gi, 'font-size:2.76333338px;')
    }
  } else if(lines.length > 1) {
    // sub text
    if(second && second.length) {
      yFirst = '63.771973';
      txt = txt.replace('Card_Text</tspan></text>', 'Card_Text</tspan></text><text xml:space="preserve" x="32.372433" y="69.522385"><tspan sodipodi:role="line" x="32.372433" y="69.522385" style="font-size:2.46944451px;line-height:1.25;font-family:serif;-inkscape-font-specification:\'serif\';text-align:center;text-anchor:middle;fill:#242424;fill-opacity:1;stroke-width:0.28222224px">' + second + '</tspan></text>');
      txt = txt.replace('line-height:125%;', 'line-height:100%;')
    }
    // second line
    txt = txt.replace('Card_Text</tspan>', 'Card_Text</tspan><tspan style="font-weight:bold;font-size:2.96333338px;line-height:100%;font-family:serif;-inkscape-font-specification:\'serif Bold\';text-align:center;text-anchor:middle;fill:#242424;fill-opacity:1;stroke-width:0.28222224px" x="32.372433" y="66.891973" sodipodi:role="line">' + lines[1] + '</tspan>');
    txt = txt.replace('Card_Text', lines[0]);
    // move text up
    txt =  txt.replace('66.271973', yFirst);
  } else {
    // sub text
    if(second && second.length) {
      yFirst = '65.271973';
      txt = txt.replace('Card_Text</tspan></text>', 'Card_Text</tspan></text><text xml:space="preserve" x="32.372433" y="68.522385"><tspan sodipodi:role="line" x="32.372433" y="69.522385" style="font-size:2.46944451px;line-height:1.25;font-family:serif;-inkscape-font-specification:\'serif\';text-align:center;text-anchor:middle;fill:#242424;fill-opacity:1;stroke-width:0.28222224px">' + second + '</tspan></text>');
      txt = txt.replace('line-height:125%;', 'line-height:100%;')
      // move text up
      txt =  txt.replace('66.271973', yFirst);
    }
    txt = txt.replace('Card_Text', main);
  }
  if(smallText) {
    txt = txt.replace(/font-size:2.96333338px;/gi, 'font-size:2.46333338px;')
  }
  return txt;
}


/**
 * Add date text to svg card.
 * @param content svg content
 * @param date date (year)
 */
function putDate(content, date) {
  let txt = content;
  if(date && date.length > 10) {
    txt = txt.replace('font-size:3.52777791px;', 'font-size:2.72777791px;');
  }
  txt = txt.replace('Card_Date', date);
  return txt;
}

/**
 * Class used to maintain cards creation context.
 */
class Context {
  #basePath = '';
  #idx = 0;
  #templateName = 'base';
  #template = null;
  #template2 = null;
  #rl = null;

  constructor() {
  }

  get template() {
    return this.#templateName
  }

  set template(name) {
    let frontTemplate = path.join('templates', this.#templateName + '-template.svg');
    let backTemplate = path.join('templates', this.#templateName + '-template2.svg');
    if(!fs.existsSync(frontTemplate)) {
      console.error('Missing card front template', frontTemplate);
      this.#templateName = '';
      return;
    }
    if(!fs.existsSync(backTemplate)) {
      console.error('Missing card back template', frontTemplate);
      this.#templateName = '';
      return;
    }
    this.#templateName = name
  }

  get path() {
    return this.#basePath;
  }

  set path(basePath) {
    let frontTemplate = path.join('templates', this.#templateName + '-template.svg');
    let backTemplate = path.join('templates', this.#templateName + '-template2.svg');
    let src = path.join(basePath, 'data.csv');
    let fileStream;

    if(!fs.existsSync(basePath)) {
      console.error('Deck does not exists.', basePath);
      return;
    }

    this.#basePath = basePath;
    this.#template = fs.readFileSync(frontTemplate, 'utf8');
    this.#template2 = fs.readFileSync(backTemplate, 'utf8');
    if (!fs.existsSync(src)) {
      console.error('Missing source data', src);
    }
    fileStream = fs.createReadStream(src);
    this.#rl = readline.createInterface({
      input: fileStream,
      crlfDelay: Infinity // recognize all instances of CR LF
    });
  }

  async generate() {
    if(!this.#basePath || !this.#basePath.length) {
      console.error('Missing deck (context path, directory).');
      return;
    }
    let distDir = 'dist-' + this.#templateName;
    if(!fs.existsSync(path.join(this.#basePath, distDir))) {
      console.info('Create destination directory', path.join(this.#basePath, distDir));
      fs.mkdirSync(path.join(this.#basePath, distDir));
    }
    for await (const line of this.#rl) {
      if(!line || !line.length) { continue; }
      const elt = line.split(';');
      const filename = ('0' + this.#idx).slice(-2) + '-' + formatName(elt[0]);
      const date = elt[1]
      const src = path.join(this.#basePath, 'img', elt[2]);
      const secondaryText = elt[3];
      const options = elt[4];
      const out1 = path.join(this.#basePath, distDir, filename + '_recto.png');
      const out2 = path.join(this.#basePath, distDir, filename + '_verso.png');
      const tmpSvg = path.join(this.#basePath, '.tmp.svg');
      let content
      let image;
      let format;

      console.info('src', src);
      this.#idx++;
      if (fs.existsSync(out1)) {
        console.info('\nFile exists', out1, 'skip export for', elt[0]);
        continue;
      }

      console.info('\nExport', elt[0]);
      console.info('from', src, 'name', filename);
      image = new Buffer(fs.readFileSync(src)).toString('base64');
      format = 'image/jpeg'; // TODO image format
      content = putText(this.#template, elt[0], secondaryText, options);
      content = content.replace('Card_Image', 'data:' + format + ';base64,' + image);
      fs.writeFileSync(tmpSvg, content, 'utf8');
      await exportCard(tmpSvg, out1);

      content = putText(this.#template2, elt[0], secondaryText, options);
      content = putDate(content, date)
      content = content.replace('Card_Image', 'data:' + format + ';base64,' + image);
      fs.writeFileSync(tmpSvg, content, 'utf8');
      await exportCard(tmpSvg, out2);

      console.info('Export done for', elt[0]);
    }
    // fs.unlink(tmpSvg);
    console.info('\nGeneration done in', this.#basePath, 'with', this.#templateName, 'template');
  }
}

let context = new Context();

if(process.argv.length < 3) {
  console.error("Missing arguments: Usage \"node build.js deckName\"");
  console.error("  with deckName: deck name (folder) in ./decks directory.");
}

// context.template = 'base';
context.template = 'miniEuropean';
context.path = 'decks/' + process.argv[2];
context.generate();
